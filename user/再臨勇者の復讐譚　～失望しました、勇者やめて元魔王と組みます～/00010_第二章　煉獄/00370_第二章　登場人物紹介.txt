・天月伊織/アマツ

　かつて仲間に裏切られた、伝説の勇者。
　三十年後の未来に召喚され、自身を裏切った者達へ復讐を誓う。

　連合国で、マーウィンとベルトガに凄惨な復讐を果たす。
　その度に「ははははっ!」と笑っているが、SAN値が削られている模様。

　他人に過度な期待を抱いていたため、裏切られた反動で極端に人を疑うようになる。
　冒険者達が裏切らなかった事実に困惑し、自分を見直す切っ掛けとなった。
　今回の一件で、自分に人を見る目がないことを自覚する。


頭:なし
右腕:強魔の指輪
左腕:防魔の腕輪
胴部:紅蓮の鎧
脚部:軽いブーツ
武器:翡翠の太刀
使用魔術:魔毀封殺、魔技簒奪、魔撃反射


・エルフィスザーク・ギルデガルド

　元魔王。

　オルテギアによって部下を皆殺しにされ、自身も体を五つに切り分けられて封印されてしまう。
　部位は『頭』『両腕』『両足』『胴体』『心臓』で分けられている。
　現在は『頭』と『両腕』を取り戻した。

　気の抜けた所をよく見せるが、敵に対する冷酷さも持ち合わせている。


頭:なし
右腕:温泉まんじゅう
左腕:温泉まんじゅう
胴部:黒のドレス
脚部:黒のブーツ
武器:なし
使用魔術:魔王眼


連合国

・ミーシャ

　猫人種の女性。
　Bランクの冒険者をやっている。

　魔族と人間の戦争に巻き込まれ、猫人種の集落を追われる。
　妹と共に温泉都市にやってきて、途方にくれている所でゾォルツに拾われる。
　そのため、ゾォルツには非常に感謝している。

　何度も助けられたことで、伊織を好意的に思う。
　そのため、伊織が街を出て行ってしまい、落ち込んでいる。

　訛りで語尾に『ニャ』と付けないよう、常に気を張っている。


頭:護りのイヤーカフス
右腕:炎龍の籠手
左腕:炎龍の籠手
胴部:ドラゴンメイル
脚部:キャットブーツ
武器:バゼラート×2


・ニャンメル

　猫人種の少女。
　ミーシャの妹。

　ミーシャと共に温泉都市に流れ着き、ゾォルツに拾われる。
　それ以来、鍛冶屋の手伝いをしている。

　訛りである『ニャ』を隠すこと無く使っていく。
　そのため、冒険者にファンが多い。


頭:なし
右腕:なし
左腕:なし
胴部:接客用制服
脚部:動きやすい靴
武器:『ニャ』という口調


・ゾォルツ

　鍛冶屋の店主。

　過去に伊織に救われており、その時に言われた言葉をずっと胸に閉まっている。
　ミーシャとニャンメルを助けたのはその為。

　頑固でぶっきらぼうな性格だが、鍛冶の腕は確か。
　翡翠の太刀と紅蓮の鎧を作り、伊織に渡した。


・ゾルキン・フィンガス

　Aランク冒険者。

　意味ありげに出てきたが、実はただの人間。
　放魔症という常時魔力を放出してしまう病気に掛かっており、それを封じる鎧を身に着けている。
　全力を出す時はそれを脱ぐ。

　子供が二人いたが、どちらも冒険者になって死んでしまった。
　そのため、子供が冒険者になることを嫌っている。
　そして何より、子供が死ぬ所を見るのが一番嫌い。


・マーウィン・ヨハネス

　人狼種。

　優れた結界魔術師。
　戦闘力はあまり無い。

　三十年前、自分の保身の為に伊織を裏切ろうとした。
　それが露見することを恐れ、伊織の殺害に協力する。

　魔王軍の襲撃から連合国を守って以来、温泉都市を拠点に好き勝手してきた。
　議員、冒険者ギルド職員とも繋がりがある。

　リューザスから伊織達の事を耳にし、誘き出す為にゾォルツの店を燃やす。
　結界を張り、部下を揃えて伊織達を待ち構えるが、あっさりと突破される。
　そして伊織の演技によって心をへし折られ、絶望の中で焼き殺された。


頭:なし
右腕:シルバーリング
左腕:なし
胴部:藍色のスーツ
脚部:黒の革靴
武器:なし
使用魔術：魔封じの結界、魔奪の結界、火炎弾



・ゴードン

　人狼種。
　Bランク冒険者。

　人狼種であることを誇り、他種族を見下している。
　マーウィンの手下で、伊織を監視していたがエルフィの魔眼で消し炭にされた。



魔王軍

・ベルトガ

　炎を使いこなす炎鬼。
　強い者には媚び、弱い者にはとことん強気に出る。
　三十年前に伊織を騙し、猛毒の『鬼の爪』を飲ませた裏切り者の一人。

　ディオニスの部下で、あれこれと使われていた。
　ギルドから討伐隊の資料を盗み、マーウィンと繋がりを持っている。
　魔力付与品を使い、冒険者の中に紛れ込んでいた。

　実力者ではあるが、力に自信を持っているため隙が多い。
　炎魔将を見下していたが、実際は炎魔将よりも弱い。

　過去に人間に見下されていたのを相当根に持っており、事あるごとに『格』という単語を口にする。
　そのため、伊織からは"格格おじさん"などと呼ばれてしまった。

　瀕死のダメージを負うも、伊織からポーションを受け取って回復。
　即座に裏切ろうとするも、ポーションに入っていった鬼の爪の効果でもがき苦しむ、
　伊織を裏切ったことを心底後悔し、毒の効果で死亡。


頭:人化の兜
右腕:なし
左腕:なし
胴部:破戒の鎧
脚部:炎鬼・具足
武器:煉獄の金棒
使用魔術：鬼神の威容、灼風


・炎魔将

　炎巨人の希少種。
　個体名はゴルギス。
　ダメージを負うと体を黒くし、魔術の威力を強化する。

===============================================

二章完結です。
三章に関しては、出来るだけ早めに投稿していきます。
