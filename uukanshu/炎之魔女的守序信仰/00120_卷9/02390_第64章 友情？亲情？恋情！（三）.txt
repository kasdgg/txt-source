艾文视角——

虽然这段时间是无所事事的休假，但是修炼并不能停。

我紧握着自己的爱用剑练习着空挥。

动作方面早已牢牢记在身体上了。

长剑的长度一般在60~66英寸之间。

高手会定制更长的剑。

更长虽然有了距离上的优势，但是却难以使用。
》m
克莉斯所使用的细剑则要短上一些，但是轻便迅速，适合女孩和体型小的剑士。

一般来说最强的是使用大剑，双手剑的流派。

但是能够握起那样的剑就已经相当费力了，沉重的剑身容易导致身体失衡并且难以闪避。

也不像是长剑那样可以在另一只手上持盾防御。

这把剑现在对於我来说稍微有点短了，也轻了一些……

是不是考虑要换一把剑了呢。

我看着自己的手掌。

比起一开始刚跟父亲学习剑术的时候真是天差地别。

但是对於剑士来说是有着极限的。

父亲当初作为冒险者的时候曾经以刃狮的外号而出名。

但是在三年前的时候我已经能击败他了。

就算年龄大了，动作变慢了也不可能输给握剑数年的小鬼的。

我现在清楚地认识到了这一点。

这就是父亲的极限……

“我的极限又在哪裡呢？”

和冰龙战鬥时幾次自己爆發出了奇怪的力量。

虽然异常强大却难以控制……

露芙特将其称为是萨塔纳吉亚()的力量。

自己并非没有好奇心，针对这个问题曾经向学院长询问了数次。

然而她只丢给我“我也不知道。”的答案。

不仅如此，希儿的创世之钥中也得不到自己想要的回答。

“我究竟又是谁呢……为什麼会拥有这样的力量啊。”

在思考这些事情的时候。花园裡有人走了过来。

“下午好，小艾文~~一直在这裡练剑吗？”

“啊……是大哥啊。”

即使在平时也是一身正装并且对谁都露出完美的笑容。

我的哥哥。法莱恩……

从小开始就对他没什麼好感。

不仅仅是哥哥非常优秀的原因，而是无论何时他都从容不迫的这副样子。

虽然对谁都彬彬有礼。但是我非常清楚，他对谁的事情都没有真正地关心过。

我一点也看不透这傢伙的想法。

“怎麼了，一脸闷闷不乐的样子？那两位可爱的小姐没在你的身边吗？”

他说的是克莉斯和希儿吧。

很不巧，她们今天出门了。

“女生们都一起出去逛街了哟，妈妈也很开心地一起跟出去了……”

“难怪，就连琳达小姐也出门了呢。”

好像是昨天她们下定的注意，这样也好，许久不见的话克莉斯一定对大家有很多想说的话吧。

希儿也愿意一同前往倒是出乎了我的预料，还以为她一定会拒绝的。

嘛~~不是很好吗。

“父亲在书房吗？”

哥哥拂了一下他那微卷的头髮。

“从早上开始就在那裡了。透过窗子也能看到艾文挥剑的样子呢~~”

他微微一笑。

“父亲和艾文说的话你不再考虑一下吗？”

如此被询问了，但是我不太明白哥哥指的是什麼。

“什麼话？”

“成为家主的事情哟……”

哥哥毫不顾忌地说了出来。

一般来说在贵族的兄弟间这可是禁句啊。

哥哥成为家主的话我就会成为分家的家主。

反之则亦然。

然而对於父亲这样只有一块领地的边缘贵族来说兄弟之间更是有一方会没落下去。

虽然哥哥是已经被国王陛下封赏了新领地的臣子。

但这并不意味着特罗洛普就是属於我的。

哥哥如果说想要继承这个莎法尔家的领地的话我也只能服从。

不仅仅是因为他是哥哥。

更大的原因是才能上吧……

练剑可以通过时间磨练技术，但是当领主则不行。

稀里糊涂一天，领民们就会受一天的苦。

很抱歉，我是没有当领主的才能的。

现在的自己，视线无法从克莉斯和希儿身上离开。

我对着哥哥摇了头。

“比起不成熟的我，哥哥要更加合适。”

听到我的回答他稍稍吃了一惊。

“真出乎我的意料……那个任性的小艾文居然会说出这样的话。”

人是会随着时间改变的啊。

“以前的我对领主也没什麼兴趣，不如说不了解这到底要承担怎麼样的责任……既然做了就要负责到底，直到死也要全力去守护。这可是我的信条之一哟。”

我将剑插回鞘中，对着他一笑。

哥哥愣了一会，相对着我大声笑了出来。

“哈哈哈，真是……太後悔了。这麼晚才回到这个家裡……要是前两年看到你这个样子我大概就能完全放心了吧。”

“回来的时候对我的事情有所担心吗？”

“当然，毕竟是我的弟弟啊……不过现在我已经理解了你的想法了。”

哥哥轻拍了一下我的肩膀。

“但是我还是建议艾文你继承这个莎法尔家呢。”

他少有地睁大了眼睛，以极其认真的态度按着我的双肩。

“为什麼？”

这样的哥哥。我只见过一次……

小时候我自己出去冒险时他救我回来时才露出过这样的表情。

“成了领主之後，就算你有两个妻子你也有可以让她们过上无憂无虑生活的资本哟……”

“唔……”

我从来没有考虑到这个问题。

克莉斯也许过了幾年女仆的生活也做过冒险者。但是希儿还是个娇贵的公主。

冒险者的收入是不稳定的。也不能做一辈子。而且非常危险。即使再强的人也有在遇到强敌时大意结果丢掉性命的例子。

真正的守护并不是用剑……而是要让她们待在安全的地方。

可是那两个人会同意吗？

就这样成为我的妻子，在这块地方一直平稳地生活下去。

“大哥说的我明白……但是……”

“你现在大概无法理解吧。不必这样立刻作出决定也可以，父亲那边我会去说的，如果感到疲惫的话就回来吧，不要太勉强自己和她们了。”

“我明白了……”

重新换上微笑之後，哥哥挥了挥手离开了。

然而在他快到宅邸的门前时好像突然想了什麼一样回过了头。

“对了，差点把最重要的事情忘记了……”

“诶？”

“听好了，艾文。虽然是父亲说过的话了，但是我要充分地解释一下……那个地方绝对不能去……”

◆◇

此时在特罗洛普的城门口处。

“下一个！”

守城的士兵开始忙碌了起来。

虽然平时这个点也是较为繁忙的，但是这两天的工作还是过於繁重了。

通过这裡的商人数量增加。每个都带着异常多的行李。

这并不像是经商，反而有一种拖家带口搬家的感觉。

“喂，你们没有许可证，不能通过！”

就在她们队伍的前面，一队车队被拦了下来。

领队的男人看上去十分慌张，就像逃出来一样。

赫蒂从後面望着这队人马，和周围的人们一起将视线的焦点放在他们的身上。

“别这样说啊！大人！我们是好不容易才离开希尔芙利亚的！！再呆在那裡肯定会没命的！”

希尔芙利亚？！

祖国的名字出现在她的耳边让赫蒂无法只做一个围观者。

她一跃跳下独角兽，显眼的容貌和坐骑让视线分到了赫蒂的身上。

“你刚才说的是怎麼一回事？希尔芙利亚怎麼了！”

身为公主的她此刻也无法保持优雅和矜持。

拼命摇起了那个高了近乎她一个头的那个男人的肩膀。

“你，你是谁啊……我什麼也不知道！”

“在下是希尔芙利亚的第一王女。赫蒂！身为希尔芙利亚的子民敢在王女面前有所隐瞒，我可以立刻斩下你的脑袋！”

说着赫蒂从背後拔出了那把巨大的双手剑。

“赫，赫蒂殿下！？为什麼会在……”

“你只需回答我的问题！”

她的架势在骑士们看来并没有杀气，但是对於外行人来说那巨剑已经足够让人害怕了。

“我知道。我知道了，赫蒂殿下……其实我也不了解详细的情况，但是凯恩国王陛下好像暗藏魔女。这件事情已经被传开了！然而之前路修卡利亚的使者来访的时候陛下完全拒绝接待他了，所以这不仅引起了圣国与联盟各国的不满……还让本国以亚斯艾尔为首的贵族们举起了反旗！这样下去各国的攻入也是迟早……”

男人的话让赫蒂呆在了原地。

在一旁的加西亚保持着冷静接着赫蒂的後面问道。

“这是什麼时候的事情？”

“两个月之。之前！”

“我们在旅途中完全没听过这类的信息，公主殿下……不可轻信。”

加西亚单膝跪地向着赫蒂谏言。

但是商人却补充了。

“我。我没有说谎啊！枫之商会以他们强大的势力帮助国王陛下封锁着国内的消息……可能是圣国还没有發出檄文的原因所以大家才不知道吧……大人！我真的没开玩笑啊！圣国的军队还好，如果是贪婪的联盟国希尔芙利亚一定会生灵涂炭的，为了逃出来都快耗尽我全部的财产了！”

赫蒂差点瘫坐在地上。

怎麼可能，天下无双的贸易之国怎麼会？

父王他暗藏了魔女……

怎麼可能啊？

自己在以诺大人的身边时并没有听说这样的事情啊，难道是自己离开之後才發生的吗？

想要找以诺大人问个清楚。

但是父王那边又是，现在到底该先去哪裡！

“对了，传闻中的魔女究竟是谁？！我身为王女从未听说过这样的事情！”

“不，不知道！但是大家传闻说是王妃塞西莉亚大人！”

“什！！”

赫蒂变得踉踉跄跄的差点摔倒在地。

加西亚只好扶住了她。

“殿下！振作点，这个庶民的话没有真实性。”

然而赫蒂却没有理睬他的话，径直跳上了坐骑。

“加西亚，回去！回希尔芙利亚！！”

没有等待，全身板甲的公主驱驰着独角兽离开了这块边境的土地。(未完待续……)