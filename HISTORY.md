# HISTORY

## 2019-05-16

### Epub

- [誰にでもできる影から助ける魔王討伐](user_out/%E8%AA%B0%E3%81%AB%E3%81%A7%E3%82%82%E3%81%A7%E3%81%8D%E3%82%8B%E5%BD%B1%E3%81%8B%E3%82%89%E5%8A%A9%E3%81%91%E3%82%8B%E9%AD%94%E7%8E%8B%E8%A8%8E%E4%BC%90) - user_out
  <br/>( v: 15 , c: 72, add: 10 )
- [平凡職業造就世界最強](wenku8_out/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8_out
  <br/>( v: 27 , c: 464, add: 2 )

### Segment

- [平凡職業造就世界最強](wenku8/%E5%B9%B3%E5%87%A1%E8%81%B7%E6%A5%AD%E9%80%A0%E5%B0%B1%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7) - wenku8
  <br/>( s: 72 )

## 2019-05-15

### Epub

- [黑之魔王](user_out/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B) - user_out
  <br/>( v: 31 , c: 582, add: 3 )

## 2019-05-14

### Epub

- [食銹末世錄](dmzj/%E9%A3%9F%E9%8A%B9%E6%9C%AB%E4%B8%96%E9%8C%84) - dmzj
  <br/>( v: 1 , c: 23, add: 23 )
- [魔法師塔塔](dmzj/%E9%AD%94%E6%B3%95%E5%B8%AB%E5%A1%94%E5%A1%94) - dmzj
  <br/>( v: 1 , c: 12, add: 12 )
- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( v: 1 , c: 108, add: 1 )
- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 1 , c: 17, add: 1 )
- [ポンコツ少女の電脳世界救世記](girl/%E3%83%9D%E3%83%B3%E3%82%B3%E3%83%84%E5%B0%91%E5%A5%B3%E3%81%AE%E9%9B%BB%E8%84%B3%E4%B8%96%E7%95%8C%E6%95%91%E4%B8%96%E8%A8%98) - girl
  <br/>( v: 1 , c: 13, add: 1 )
- [王女殿下はお怒りのようです](girl/%E7%8E%8B%E5%A5%B3%E6%AE%BF%E4%B8%8B%E3%81%AF%E3%81%8A%E6%80%92%E3%82%8A%E3%81%AE%E3%82%88%E3%81%86%E3%81%A7%E3%81%99) - girl
  <br/>( v: 1 , c: 26, add: 26 )
- [俺が淫魔術で奴隷ハーレムを作る話](h/%E4%BF%BA%E3%81%8C%E6%B7%AB%E9%AD%94%E8%A1%93%E3%81%A7%E5%A5%B4%E9%9A%B7%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%82%92%E4%BD%9C%E3%82%8B%E8%A9%B1) - h
  <br/>( v: 1 , c: 6, add: 2 )
- [願わくばこの手に幸福を](syosetu/%E9%A1%98%E3%82%8F%E3%81%8F%E3%81%B0%E3%81%93%E3%81%AE%E6%89%8B%E3%81%AB%E5%B9%B8%E7%A6%8F%E3%82%92) - syosetu
  <br/>( v: 1 , c: 1, add: 1 )
- [望まぬ不死の冒険者](syosetu_out/%E6%9C%9B%E3%81%BE%E3%81%AC%E4%B8%8D%E6%AD%BB%E3%81%AE%E5%86%92%E9%99%BA%E8%80%85) - syosetu_out
  <br/>( v: 14 , c: 350, add: 43 )
- [自称贤者弟子的贤者](ts_out/%E8%87%AA%E7%A7%B0%E8%B4%A4%E8%80%85%E5%BC%9F%E5%AD%90%E7%9A%84%E8%B4%A4%E8%80%85) - ts_out
  <br/>( v: 7 , c: 313, add: 1 )
- [俺の死亡フラグが留まるところを知らない](user_out/%E4%BF%BA%E3%81%AE%E6%AD%BB%E4%BA%A1%E3%83%95%E3%83%A9%E3%82%B0%E3%81%8C%E7%95%99%E3%81%BE%E3%82%8B%E3%81%A8%E3%81%93%E3%82%8D%E3%82%92%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84) - user_out
  <br/>( v: 5 , c: 107, add: 2 )
- [転生ごときで逃げられるとでも、兄さん？](yandere_out/%E8%BB%A2%E7%94%9F%E3%81%94%E3%81%A8%E3%81%8D%E3%81%A7%E9%80%83%E3%81%92%E3%82%89%E3%82%8C%E3%82%8B%E3%81%A8%E3%81%A7%E3%82%82%E3%80%81%E5%85%84%E3%81%95%E3%82%93%EF%BC%9F) - yandere_out
  <br/>( v: 5 , c: 115, add: 4 )

### Segment

- [食銹末世錄](dmzj/%E9%A3%9F%E9%8A%B9%E6%9C%AB%E4%B8%96%E9%8C%84) - dmzj
  <br/>( s: 23 )
- [魔法師塔塔](dmzj/%E9%AD%94%E6%B3%95%E5%B8%AB%E5%A1%94%E5%A1%94) - dmzj
  <br/>( s: 12 )
- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( s: 1 )
- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( s: 1 )
- [王女殿下はお怒りのようです](girl/%E7%8E%8B%E5%A5%B3%E6%AE%BF%E4%B8%8B%E3%81%AF%E3%81%8A%E6%80%92%E3%82%8A%E3%81%AE%E3%82%88%E3%81%86%E3%81%A7%E3%81%99) - girl
  <br/>( s: 26 )
- [俺が淫魔術で奴隷ハーレムを作る話](h/%E4%BF%BA%E3%81%8C%E6%B7%AB%E9%AD%94%E8%A1%93%E3%81%A7%E5%A5%B4%E9%9A%B7%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%82%92%E4%BD%9C%E3%82%8B%E8%A9%B1) - h
  <br/>( s: 2 )
- [願わくばこの手に幸福を](syosetu/%E9%A1%98%E3%82%8F%E3%81%8F%E3%81%B0%E3%81%93%E3%81%AE%E6%89%8B%E3%81%AB%E5%B9%B8%E7%A6%8F%E3%82%92) - syosetu
  <br/>( s: 1 )

## 2019-05-13

### Epub

- [レベル１の最強賢者](syosetu/%E3%83%AC%E3%83%99%E3%83%AB%EF%BC%91%E3%81%AE%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85) - syosetu
  <br/>( v: 4 , c: 37, add: 17 )

### Segment

- [レベル１の最強賢者](syosetu/%E3%83%AC%E3%83%99%E3%83%AB%EF%BC%91%E3%81%AE%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85) - syosetu
  <br/>( s: 15 )
- [俺の死亡フラグが留まるところを知らない](user/%E4%BF%BA%E3%81%AE%E6%AD%BB%E4%BA%A1%E3%83%95%E3%83%A9%E3%82%B0%E3%81%8C%E7%95%99%E3%81%BE%E3%82%8B%E3%81%A8%E3%81%93%E3%82%8D%E3%82%92%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84) - user
  <br/>( s: 30 )

## 2019-05-12

### Epub

- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( v: 1 , c: 107, add: 4 )
- [ポンコツ少女の電脳世界救世記](girl/%E3%83%9D%E3%83%B3%E3%82%B3%E3%83%84%E5%B0%91%E5%A5%B3%E3%81%AE%E9%9B%BB%E8%84%B3%E4%B8%96%E7%95%8C%E6%95%91%E4%B8%96%E8%A8%98) - girl
  <br/>( v: 1 , c: 12, add: 1 )
- [傀儡契約_(n4719ff)](girl/%E5%82%80%E5%84%A1%E5%A5%91%E7%B4%84_(n4719ff)) - girl
  <br/>( v: 1 , c: 8, add: 2 )
- [原千金未婚媽媽](girl/%E5%8E%9F%E5%8D%83%E9%87%91%E6%9C%AA%E5%A9%9A%E5%AA%BD%E5%AA%BD) - girl
  <br/>( v: 2 , c: 39, add: 7 )
- [豚公爵に転生したから、今度は君に好きと言いたい](user_out/%E8%B1%9A%E5%85%AC%E7%88%B5%E3%81%AB%E8%BB%A2%E7%94%9F%E3%81%97%E3%81%9F%E3%81%8B%E3%82%89%E3%80%81%E4%BB%8A%E5%BA%A6%E3%81%AF%E5%90%9B%E3%81%AB%E5%A5%BD%E3%81%8D%E3%81%A8%E8%A8%80%E3%81%84%E3%81%9F%E3%81%84) - user_out
  <br/>( v: 6 , c: 202, add: 7 )

### Segment

- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( s: 1 )
- [傀儡契約_(n4719ff)](girl/%E5%82%80%E5%84%A1%E5%A5%91%E7%B4%84_(n4719ff)) - girl
  <br/>( s: 4 )
- [原千金未婚媽媽](girl/%E5%8E%9F%E5%8D%83%E9%87%91%E6%9C%AA%E5%A9%9A%E5%AA%BD%E5%AA%BD) - girl
  <br/>( s: 2 )

## 2019-05-11

### Epub

- [幼馴染二人から告白され困惑するも勢いで３Ｐに持ちこもうとする女の子の話](girl/%E5%B9%BC%E9%A6%B4%E6%9F%93%E4%BA%8C%E4%BA%BA%E3%81%8B%E3%82%89%E5%91%8A%E7%99%BD%E3%81%95%E3%82%8C%E5%9B%B0%E6%83%91%E3%81%99%E3%82%8B%E3%82%82%E5%8B%A2%E3%81%84%E3%81%A7%EF%BC%93%EF%BC%B0%E3%81%AB%E6%8C%81%E3%81%A1%E3%81%93%E3%82%82%E3%81%86%E3%81%A8%E3%81%99%E3%82%8B%E5%A5%B3%E3%81%AE%E5%AD%90%E3%81%AE%E8%A9%B1) - girl
  <br/>( v: 1 , c: 1, add: 0 )
- [魔王の娘の百合戦記](girl/%E9%AD%94%E7%8E%8B%E3%81%AE%E5%A8%98%E3%81%AE%E7%99%BE%E5%90%88%E6%88%A6%E8%A8%98) - girl
  <br/>( v: 1 , c: 9, add: 0 )

## 2019-05-10

### Epub

- [chase　～何度ループしてもあなたを救えない物語～](girl/chase%E3%80%80%EF%BD%9E%E4%BD%95%E5%BA%A6%E3%83%AB%E3%83%BC%E3%83%97%E3%81%97%E3%81%A6%E3%82%82%E3%81%82%E3%81%AA%E3%81%9F%E3%82%92%E6%95%91%E3%81%88%E3%81%AA%E3%81%84%E7%89%A9%E8%AA%9E%EF%BD%9E) - girl
  <br/>( v: 1 , c: 1, add: 1 )
- [幼馴染二人から告白され困惑するも勢いで３Ｐに持ちこもうとする女の子の話](girl/%E5%B9%BC%E9%A6%B4%E6%9F%93%E4%BA%8C%E4%BA%BA%E3%81%8B%E3%82%89%E5%91%8A%E7%99%BD%E3%81%95%E3%82%8C%E5%9B%B0%E6%83%91%E3%81%99%E3%82%8B%E3%82%82%E5%8B%A2%E3%81%84%E3%81%A7%EF%BC%93%EF%BC%B0%E3%81%AB%E6%8C%81%E3%81%A1%E3%81%93%E3%82%82%E3%81%86%E3%81%A8%E3%81%99%E3%82%8B%E5%A5%B3%E3%81%AE%E5%AD%90%E3%81%AE%E8%A9%B1) - girl
  <br/>( v: 1 , c: 1, add: 1 )
- [回復術士のやり直し～即死魔法とスキルコピーの超越ヒール～](user_out/%E5%9B%9E%E5%BE%A9%E8%A1%93%E5%A3%AB%E3%81%AE%E3%82%84%E3%82%8A%E7%9B%B4%E3%81%97%EF%BD%9E%E5%8D%B3%E6%AD%BB%E9%AD%94%E6%B3%95%E3%81%A8%E3%82%B9%E3%82%AD%E3%83%AB%E3%82%B3%E3%83%94%E3%83%BC%E3%81%AE%E8%B6%85%E8%B6%8A%E3%83%92%E3%83%BC%E3%83%AB%EF%BD%9E) - user_out
  <br/>( v: 8 , c: 167, add: 21 )

## 2019-05-09

### Epub

- [惡役轉生但是為什麼會變成這樣](girl/%E6%83%A1%E5%BD%B9%E8%BD%89%E7%94%9F%E4%BD%86%E6%98%AF%E7%82%BA%E4%BB%80%E9%BA%BC%E6%9C%83%E8%AE%8A%E6%88%90%E9%80%99%E6%A8%A3) - girl
  <br/>( v: 10 , c: 148, add: 148 )
- [黑之魔王](user_out/%E9%BB%91%E4%B9%8B%E9%AD%94%E7%8E%8B) - user_out
  <br/>( v: 31 , c: 579, add: 6 )

## 2019-05-08

### Epub

- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( v: 1 , c: 103, add: 3 )
- [「「神と呼ばれ、魔王と呼ばれても」」](girl/%E3%80%8C%E3%80%8C%E7%A5%9E%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%80%81%E9%AD%94%E7%8E%8B%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%81%A6%E3%82%82%E3%80%8D%E3%80%8D) - girl
  <br/>( v: 1 , c: 72, add: 11 )
- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 1 , c: 16, add: 1 )
- [ポンコツ少女の電脳世界救世記](girl/%E3%83%9D%E3%83%B3%E3%82%B3%E3%83%84%E5%B0%91%E5%A5%B3%E3%81%AE%E9%9B%BB%E8%84%B3%E4%B8%96%E7%95%8C%E6%95%91%E4%B8%96%E8%A8%98) - girl
  <br/>( v: 1 , c: 11, add: 1 )
- [十歳の最強魔導師](girl/%E5%8D%81%E6%AD%B3%E3%81%AE%E6%9C%80%E5%BC%B7%E9%AD%94%E5%B0%8E%E5%B8%AB) - girl
  <br/>( v: 1 , c: 146, add: 5 )
- [村民轉生武器使](girl/%E6%9D%91%E6%B0%91%E8%BD%89%E7%94%9F%E6%AD%A6%E5%99%A8%E4%BD%BF) - girl
  <br/>( v: 1 , c: 6, add: 1 )
- [魔王の娘の百合戦記](girl/%E9%AD%94%E7%8E%8B%E3%81%AE%E5%A8%98%E3%81%AE%E7%99%BE%E5%90%88%E6%88%A6%E8%A8%98) - girl
  <br/>( v: 1 , c: 9, add: 2 )

### Segment

- [「「神と呼ばれ、魔王と呼ばれても」」](girl/%E3%80%8C%E3%80%8C%E7%A5%9E%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%80%81%E9%AD%94%E7%8E%8B%E3%81%A8%E5%91%BC%E3%81%B0%E3%82%8C%E3%81%A6%E3%82%82%E3%80%8D%E3%80%8D) - girl
  <br/>( s: 71 )
- [十歳の最強魔導師](girl/%E5%8D%81%E6%AD%B3%E3%81%AE%E6%9C%80%E5%BC%B7%E9%AD%94%E5%B0%8E%E5%B8%AB) - girl
  <br/>( s: 2 )
- [村民轉生武器使](girl/%E6%9D%91%E6%B0%91%E8%BD%89%E7%94%9F%E6%AD%A6%E5%99%A8%E4%BD%BF) - girl
  <br/>( s: 6 )



