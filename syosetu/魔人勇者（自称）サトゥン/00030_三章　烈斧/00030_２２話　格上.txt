目光都被遮住的男人嘴裡吐出的話语，让利安和玛丽薇尔还没开口之前就已操起了自己的傢伙。
那并非意味着把要这男人当做需要打倒的敌人般的存在。两人之所以要这麼做，只是生存本能所作出的自我保护罢了。
眼前的男人放出重压，他们为了自保，才会这麼做。那个气息，利安很清楚，玛丽薇尔也很清楚。
这个男人身上缠绕着的气息，和他们的师傅梅娅非常相似。并非撒东那样天衣无缝般的自身的强大，而是全身上下都汇集着经过锤炼的纯粹的技巧那种强大。
利安吞了口口水，观察着眼前的男人。尽管对方只是站在那裡，但作为武人的重压和毫无破绽的感觉，在平时老是被梅娅完虐的他可是铭记在心的。
手上拿着的，与其说是锈迹斑斑的铁斧，感觉更像打倒过成千上万魔物的勇者之斧。
所谓强者，通常都擅长於将他人眼中的自己放大。当对手看不穿自身的时候，就掌握不了自己的实力了。（PS：原文：ここまで対峙者へ自身を大きく見せるとは、一体どれほどの強者なのか。这句按照原文我翻不出我理解出来的意思，姑且按照自己理解来翻，和原文有出入，大致上应该是表达让别人评价自己实力过高，就不知道自己实力有多深了）

利安就这样握着枪向眼前的男人发问。
[要我们离开，是什麼意思？我们到这前面有事要办……]（利安）

[就像字面上的意思。在这前方有着什麼，应该清楚吧？会死的哦，少年。如果如果说什麼都不知道的話，那就护送你们到山脚下。还不想死的話，就快点返回吧]
[突然冒出来说让人【返回】什麼的，到底会有多少人会老老实实回去啊。让开，我们找这座山前方那傢伙有事]（玛丽薇尔）

[气势倒是挺让人欣赏。但是，不好意思此路不通。我再也不想见到去挑战那傢伙，然後白白送掉性命的人了]
[你的想法我不管。要说此路不通的話，就算用强的也要通过哦？反正也没有坐下好好谈一谈的意思吧？]（玛丽薇尔）

[放弃吧。我知道你们是相当的好手。那麼，就更应该明白吧？——你们，打不过我。然後，连我都打不过的話，碰上潜伏在这深處的【冰蛇】，连抵抗都做不到，只能被其蹂躏罢了。]
男人把右手拿着的双手斧扛在肩上，淡淡地说。
对於表现出不管说什麼都不让通过的意志的男人，玛丽薇尔轻轻咂舌，将视线转向撒东那边。
姑且将最终判断交给他，毕竟在这队伍裡撒东还是领头的样子。
由於玛丽薇尔的眼神，撒东一边好像在思考着的样子，一边嘀咕道。
[唔嗯，就算是爱护的人类，假如要妨碍吾的勇者大道，也要将其排除掉……但我又对利安发过誓不会杀人。咕呜呜，真令人头疼。如果是我的話，不管是什麼样的人类都只用一脚就能解决，但一脚下去那个男的不就断气了嘛！可是，伤害人类就算不上勇者了啊！明明是魔物的話不管多少都杀给你看！你这混账傢伙，太卑鄙了！为何你会是人类啊！你现在立马变成魔人就好啦嘛！对了，是那样吗？你这傢伙怕和我打，才变成人类的吧？呼哈哈哈哈！胆小鬼，真是滑稽！恼火了的話就立马别当人给我看看啊！]
[明白了，啥都干不了的勇者大人就在一边老实呆着去吧。动手吧，利安！]
[了解！]
利安和玛丽薇尔踏着厚厚的积雪向男人冲去。
跑动的同时，两人向武器传达不砍伤那个男人的意识，挥起武器。他们的武器是有意识的武器，只要主人不想伤人就能抑制住伤害。
即便如此，如果运气不好被打着实了的話免不了要受点皮肉伤，可对手也是手握武器对峙，而且还是惊人的武人。要是再下轻手，反而有会让自己陷入险境的可能。
对手的实力未知，但恐怕是和梅娅一个级别的，甚至还是那以上的好手。这等强者这边手牌暴露越多越不利。水准差太多了。
所以，两人选择了短期战。利安和玛丽薇尔，两人同时对那男人施展枪击和剑击。
两人的配合，对於敌对方来说棘手得惊人。利安的枪一击裡的怪力，玛丽薇尔的剑的出剑数，对手要同时对付这两样是非常困难的。
只顾剑的話利安会放出粉碎一切的一击，反过来顾着枪的話玛丽薇尔的剑势也很凶猛。
虽说很恶趣味，但却是打倒了魔人雷古艾斯克的两人的连击。他们自信着这攻击无论谁都挡不住。但是——那男人没那麼简单就能搞定。
男人看穿了利安和玛丽薇尔的攻击，身体降低一步半的距离压低身姿。然後手上的斧头轻轻地架到玛丽薇尔的星剑上。
星剑的轨道向右稍稍偏移，挥舞着枪的利安已进入了视野中。然而这就足够了。
产生了超高速移动的剑会落入自己枪的轨道中的错觉的利安，为了略微变更枪的轨道修正了力道，但那正中男人下怀。
将星剑稍微偏移，绕过因轨道变化而动作迟缓了的已经不再有惊人气势的利安的枪并窜入他的怀中。然後以手上双手斧的柄，全力扣入利安的胸口。
对方的攻击抢先击中了利安，肺裡的空气被挤出，脑海裏一片空白，剧痛的同时身体弯成く字。
[利安！该死！]
搭档被打垮了，玛丽薇尔为了和男人保持距离，向他追击，但那一剑却被拦住了。
男人扭转身体，改变和玛丽薇尔之间的站位，让利安挡在了中间。玛丽薇尔对男人的策略只能咬牙切齿。
这样把利安当做盾牌，不管砍哪裡都会伤到利安的吧。希望这一剑别伤到利安斩去，手腕撞上他的身体。
恐怕，这是那男人的陷阱。玛丽薇尔如果冷静地拉开距离的話，应该会就地将利安击溃吧。但，如果玛丽薇尔继续突进的話，那也会中了男人的招。
一瞬间的迷茫後，玛丽薇尔浮现出狰狞的笑向男人斩去。正好，让自己选了不再顾虑利安这一选项的这个男的，绝对不放过他。
玛丽薇尔的幻惑之剑，并非是靠力量而是靠技巧来戏弄对手的剑之舞。就像以前对阵利安那时一样，玛丽薇尔準备运用脚和剑的速度来戏弄对手，但那男人嘀咕了一句後就把这招破解了。
[还学了梅古阿克拉斯王国骑士团的剑技啊。虽然比少年还稳重一点，但还是太嫩了。]
将玛丽薇尔放出的虚招全数看穿，男人径直踏入她剑招的安全区域，锈迹斑斑的斧头刺出。
那连魔人都砍倒了的剑，被男人锈迹斑斑的斧头给一击大大拨飞。比利安还强的刚力，玛丽薇尔的脸上尽是惊愕与烦恼的苦闷表情發现了这一点，但却發现得太晚了。
她已双手空空，没有一样能护住身体的东西了。如果就那样被男人踢一脚就完了，但是。
[才不会让你，打倒玛丽薇尔呐！]
[利安！]
因疼痛歪着身体的利安把枪切入两人之间，将那一脚挡了下来。
男人没想到利安会那麼快缓过劲来，两人一边看着他那一脸不可置信的惊讶表情，一边跳着向後撤退。

但是，战况依旧恶劣。才一瞬间的交错，两人就落於下风了。看透了那点的男人，以教诲的口气谈谈地说。
[如此年轻，就锻炼到了这程度。假如积累更多经验，肯定能成为出色的战士吧。所以，回去吧。有那等才华的話，可不能让你们白白牺牲在那傢伙手上]
[别，别小看我们了。我们还，没输呐……]
[别逞强了。少年的肋骨和你的右手腕应该都收了不轻的伤。少年的肋骨可能还断掉了。你们啊，赢不了我的。那个强度和才华不错，但经验太少了。连我都赢不了的傢伙，怎麼可能打得过那个怪物]
男人说的話，让利安和玛丽薇尔只能悔恨地咬牙切齿。
因梅娅的修行，觉得变强了。明明周围的魔物都能轻鬆干掉，也有能干掉魔人的自信。
但是，眼前的男人却轻易将他们的自信给毁掉了。就算从撒东那裡拿到了的武器，也被区区一把锈迹斑斑的斧头给打倒了。
如此强大，而且还有压倒一切的技巧，玛丽薇尔不禁在内心咒骂。虽然不想承认，但有着像他那样强大的人，不正是自己心中的目标——英雄吗。
想到这裡，玛丽薇尔意识到了一点。有着这般强大力量的人类的真面目，他到底是谁。
在这个国家，还有同时与利安和玛丽薇尔交战，并且能轻鬆打倒他们的英雄吗。不对，应该有的。有一位因国王的定罪被赶下英雄宝座的悲剧的英雄。
虽然散乱的头髮遮得完全看不到的眼神。但是，玛丽薇尔却确信了。能将自己两人压倒的斧使，恐怕那个男人的真实身份是——
[——罗南的幼狮，英雄古连弗德]
玛丽薇尔嘀咕了一句，男人的脸色连变都不变一下。
但是，她却确信了。这个男人，正是悲剧的英雄古连弗德。
他是这片大陆上被誉为独一无二的好手，尤其是斧术仿佛传说中的英雄再临一般如雷贯耳。
被国王流放後，据说是去了国外了，但看情况好像是潜伏在这样的地方。
玛丽薇尔断定了他的真实身份後，向男人问道。
[你，是英雄古连弗德吧？能把我们轻易制服的斧使，除了他外也没有其他人了]（玛丽薇尔）

[……我确实叫古连弗德。但是，英雄这种东西，从来没当过。不管是过去，还是今後]（斧头男）

[你，因为那个冰蛇……那麼，拜托了！请让我们通过这裡吧！我们必须打倒那个傢伙！]（利安）

[说了你们办不到的吧。连我这样的人都赢不了，面对那怪物到底能幹啥。不管是我还是你们，都赢不了啊]（斧头男）

[怎麼觉得……从梅娅那裡听到的你和现在的样子，压根不一样啊。曾经的英雄也堕落了呢，怕冰蛇怕得啥都做不到了。办不办得到不是你能判断得了的，我们即使在和它对峙的时候也不会把剑丢下]（玛丽薇尔）

[如果和那傢伙对峙的話，你们那点能力会连話都说不出来的。行了吧，快回去]（斧头男）

[啊啊啊啊！幹嘛啊这个回去回去男！胆小鬼，懦弱，没出息！到底要唠唠叨叨到什麼程度！勇者痴还好一点啊！]（玛丽薇尔）（PS：原文勇者马鹿，即为傻傻的痴迷勇者的傢伙。）

[嗯嗯，如果是对我的讚美話什麼时候都可以倾听的哦？呼哈哈哈哈！勇者是最棒的，这当然是世界的真理啦！疼，呜啊啊啊啊啊啊！我的脑裡又响起了【去死】的集团口令啊！]（撒东）

对於一步都不打算让开的男人——古连弗德，利安和玛丽薇尔伤透了脑筋。
虽然玛丽薇尔嘴裡正说着笨蛋傢伙，但这个男人的实力可是真货。即使很怕冰蛇，但那份力量还是远在自己的上位。
所以就算试图强行突破，但以如今的利安和玛丽薇尔来还是很困难的。更何况他们身体还挂彩了。
以负伤的状态，估计是打不倒他的。虽说可以靠米蕾亚来治疗，但他应该不会放过这个机会的吧。
他们正在思考怎麼办的时候，一个男的出现在了他俩前方。那个人，当然就是撒东啦。
撒东一边活动筋骨，一边愉快地笑着向古连弗德那边放声道。
[呼哈哈哈！老老实实地听好啦，归根结底，你这傢伙肯定是想独占打倒冰蛇的攻击的吧！不行，绝对不行！冰蛇是让吾作为勇者闻名世界的重要猎物！那个猎物早在千年前，就已经被我预定啦！]（撒东）

[我是老老实实地听了……]（斧头男）

[早在千年前，你不是幾天前才从梅娅那裡听到冰蛇的情报的嘛]（玛丽薇尔）

[路人给我闭嘴！姆哈哈，想要独占勇者的猎物真是滑稽之极啊！听说，只要比你强的話就会把猎物叫出来是吧？呼哈哈哈哈哈！别担心！就让你这只是虐了利安和玛丽薇尔这样的幼仔就满足了的傢伙，看看真的强者是什麼样的吧！都说了多少次了，这次消灭冰蛇没你们两人的份！好好看着我，勇者撒东！不费吹灰之力地灭掉它！揭开故事的序幕吧！唔哈哈哈哈哈哈哈！]
撒东情绪高涨到了这地步，利安只能苦笑了，玛丽薇尔头疼地收回武器後退。
他都做到了这份上，他们就明白已经没自己的戏份了。
虽然没赢古连弗德很让人遗憾，但实际上以现在的实力还没法敌得过他也没办法。
因此，这裡先参观学习一下。两人一边接受米蕾亚的治疗，一边认真的看着撒东他们。
他的强不用说利安，连玛丽薇尔都明白，但实际上看到其与强者对峙这还是第一次。
撒东到底会用什麼方式把那个英雄打倒呢。先不管认真看着的观众们，撒东向古连弗德發出警告。
[撒，你的下个对手是我……嗯，但麻烦的是我發誓过不伤害人类的啊。我这边没法向你出手。因此我这裡有个提案不知可行否。你用你最强的杀招攻击过来就好。如果我轻鬆将其解决了，那就证明是我最强了。姆哈哈哈哈哈！咋样，别客气！撒，将你最强的一击向我打过来就是了！不是我自夸，但这千年以来，我都没见自己流过一滴血了！]
[那个，记得你前阵子刚被玛丽薇尔砍得流了一大堆血……]（米蕾亚）

[来，放马过来吧！不管什麼攻击我都不会躲开的！来来来来来来！快点出手吧！]
[抱歉，那傢伙那个背影真是烦得想让人砍一刀啊]（玛丽薇尔）

撒东像是一点也不在乎局外人的声音似的，架起手大笑等着古连弗德的一击。

可是，利安和玛丽薇尔發现古连弗德的气息变了。与两人对峙的时候，看起来心不在焉的他，在面对撒东的时候却握紧斧头并降到腰後。
那是，他认真起来了的证明。亲身体验到了撒东的强大、异常的英雄，理解到不拿出真本事的話就会被压倒的事实，因此摆好了战鬥姿势。
惊人的鬥气包住古连弗德，压迫着他周围。即使是那样怪物般的对手，撒东也轻轻鬆鬆地等待着。
古连弗德渐渐地拉近和撒东的距离。宛如野兽摸索着距离，伺机咬上一口的样子。
然後，古连弗德动手了。利安他们看到古连弗德就像字面上的【攻击】一样，那是食肉动物以超常速度捕食般的突击。
尽管利安和玛丽薇尔在已经离了一段距离的地方看，但还是只能勉强看到古连弗德用难以肉眼看清的速度向撒东逼近。
手中的斧头挥起，以最极限的精悍动作砸向撒东，然後——那斧头的前半部分消失了。
即将扎入撒东脖子的斧头，撒东以肉眼都看不清的速度动了下手，手指轻轻划过斧头根部（头和柄连接处），就这样将其向遥远的上空【斩飞】了。
不仅是古连弗德，连利安和玛丽薇尔都目瞪口呆了。撒东挺起胸，高兴地对呆了的他们说。
[呼哈哈哈哈哈！傻瓜被骗了吧！我确实说了不会伤人，但我没说不会对武器下手啊！你这傢伙自豪的斧头被我一刀两断般地打断掉了啊！现在你这傢伙只不过是拿着根棒子的山民罢了！嘎哈哈哈哈哈！以勇者为对手拿根棒子到底是要做什麼呐！还是说拿那个敲吗？嗯？敲吗？用棒子来敲勇者，真是太可怕了！呜哈哈哈哈哈哈！]
[……呐，利安。为啥，你会那麼的尊敬他呢？我有点，想要找人代替了，找那货以外的]
[撒、撒东大人非常帅气的哦？嗯，很强很帅气的哦……]
完全没有感觉到玛丽薇尔他们冷冷的视线，撒东非常得意地挺胸大笑。
但是，对战对手的古连弗德却接受了的样子，丢掉手上的棒子向撒东说了【我认输】。

本人都接受了的話，那就应该没啥问题了，这边为何会无法接受啊。玛丽薇尔烦恼地抱起了头。
不过，玛丽薇尔那心中的郁闷，很快就消散了的样子。
撒东斩飞了的斧头的前端，从遥远的上空再次落下返回地面，然後就那样漂亮地直击了勇者的後脑勺。
[嘎哈哈哈哈哈哈咕哇！]
[撒、撒东大人——！]
头部受到了强烈的一击，撒东直直向前扑倒在了积雪很深的地上。
看到了向撒东伸出援手拉他站起来的古连弗德，玛丽薇尔和米蕾亚不禁觉得，那个人，说实在的不是挺好的一个人嘛。

