
哥布林王走到了我们刚才的位置。
好像注意到痕迹一样停下了脚步。

哥布林只有人类小孩程度的大小。
但是哥布林王的身高却有一般成年男性的1.5倍程度。
身躯，腿脚和手臂都很粗壮，与其说哥布林不如说更像奥克(orc)。

艾历欧和乔希都卡嗒咔嗒地發起抖来。

哥布林只是杂鱼。一对一的话F级冒险者也不用苦战。
但是哥布林王的话，一对一即使是B级冒险者也很危险。
这是F级冒险者无论怎麼齐心协力也无法战胜的对手。

只是F级冒险者的艾历欧两人会恐惧哥布林王也是理所当然的。

顺便一提F级是新人，E级是脱离新人的程度。
D级是可以独当一面，C级是熟练，B级是一流，A级是超一流。
我的S级则是放眼整个歷史也仅仅只有幾个人被授予的等级。

我对艾历欧两人小声说道。

「冷静点」
「但是……」

F级队伍的话，跟哥布林王遭遇的那一刻就注定是全灭。
然而我是S级。

足以毁灭世界的魔神王我也单枪匹马地打倒了。
对我来说，哥布林王程度根本就不是威胁。

「要是有个万一，都交给我，艾历欧你们逃跑吧」
「什麼，怎麼能只有我们两个逃跑……」

10年前也曾经有过类似的对话。
稍微有点怀念的感觉。

「GAAAAAAA」

看来哥布林王注意到我们在附近了。
哥布林种的鼻子都很灵。是發现了我们的气味了吧。

不过看来并不知道我们具体在哪裡。或许眼神不怎麼好也说不定。

它大声吼叫的同时，大开大合地挥舞着武器。
武器是巨大的钢棒，敲在坑道墙壁上，發出巨大的响声。
岩石碎散得到处都是。

我集中注意力，观察它的举动。

哥布林王的程度我瞬间就能打倒。但是，有点不对劲。
说到能率领一大群手下的哥布林王，当然是有一定的智慧。
然而从这只哥布林王身上感觉不到知性。

一点点地从附近村子偷家畜这件事首先就令人觉得奇怪。
对缺乏知性的哥布林来说，不一下子全部偷走很奇怪。
这可是足足有30只的群落。
别说家畜，就算把村民全员抓走也不出奇。

简直就像是，为了让人误认成数量很小的哥布林群落一样。

而且坑道的入口也设置了看守。
这是哥布林们被组织起来使用着的证据。

但是眼前这只，歇斯底里地攻击墙壁的哥布林王，我不觉得能做到。
恐怕，背後还有什麼东西在。可以的话我不想让黑幕逃掉。

所以我仔细地观察着。
要是判断哥布林王不足以解决入侵者的话，黑幕会自己出来也说不定。
或者哥布林王会回去报告也有可能。
等的就是那一刻。

「GRAAAA……AAA?」

吼叫着的哥布林王突然静止了。

「咧啊啊啊啊啊」

从坑道的入口方向，有什麼人直直地冲了过来。
速度很快。一口气地接近之後，对着哥布林王连续使出了突刺。

是个兽人族的少女，而且看来是个有相当身手的战士。
细身的剑準确地向着哥布林王的弱点刺去。

「GAAA！」

用跟巨大身体不相称的敏捷速度，哥布林王做出了反应。
像树枝一样轻轻挥舞着巨大的钢棒，把少女的刺击都化解了。

「杂鱼别来碍事！」

少女巧妙地控制距离，或进或退地翻弄着哥布林王。
然而哥布林王也漂亮地应付下来了。
两者都缺乏致命的一击，一时陷入了胶着的状态。

「好厲害……」

艾历欧小声地感叹出声。这个瞬间，少女的耳朵轻轻地动了一下。

「那边的那幾个！要逃的话就趁现在！」

不愧是兽人，耳朵真灵。

「艾历欧，乔希，总之先跑吧」
「哦，哦」
「明白了」
「顺带记得注意有没有偷袭」
「恩，恩」

艾历欧他们顺着侧道冲往外面。

「GAA！」

哥布林王虽然想去追艾历欧他们，
「休想！」
少女牵制住它阻止了。

我向少女说道。

「要帮忙吗？」
「不逃跑吗！」
「因为没有逃的必要」
「随便你怎麼样。但是别妨碍到我！」

少女表情严肃地跟哥布林王战鬥着。
既然说了别妨碍的话，还是该先观望一下吧。
冒险者都有各种各样的内情。等情况不妙了再出手就好。

一时之间少女和哥布林王的战鬥势均力敌地持续着。
尽管力量是势均力敌。武器的差距却出来了。

————啪叽

發出清脆的声音，少女的剑碎了。
跟巨大的钢棒持续交锋，变成这样也是必然的。
失去武器，少女被逼入了单方面的防守。
而且没办法用武器格挡，所有攻击都只能躲避开。

我再一次问道。
「要帮忙吗？」
「不需要！你也看够了快点逃！」

她从腰间拔出短刀应战。
但是，至今为止用剑也没法打倒对方，用短刀当然也不行。
很快短刀就被打飞了。
少女慌慌张张地拉开距离，紧接着钢棒就呼啸着穿过少女刚才的位置。

「咕……」

少女悔恨地發出呻吟。
到极限了，少女被打倒也只是时间的问题吧。

「不会再问了。我就随我意插手了」
「够了快逃——」
「本来，这裡就是我先来的」

这麼说着，我把手伸向了背後的剑。
